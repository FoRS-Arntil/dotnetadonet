**Exercice : Manipulation de données avec ADO.NET et SQLite**

**Objectif :** Créer une application console C# qui utilise ADO.NET et qui utlise une base de donnée SqLite. Sur cette base de donnée il faudra effectuer des opérations de lecture et d'écriture (CRUD), et afficher les résultats.

**Étapes :**

1. **Configuration du projet :**
   - Créez un nouveau projet console C# dans votre environnement de développement préféré (Visual Studio, VS Code, etc.).
   - Ajoutez la référence nécessaire à la bibliothèque SQLite via NuGet (Cf. aux slides du cours)

2. **Création du schéma de base de données :**
   - Créez la base de donnée grâce au script de création. Le script de création de la table se trouve sur le learn et sur le template du projet.

3. **Connexion à la base de données :**
   - Établissez une connexion à votre base de données SQLite en utilisant la classe `SQLiteConnection`.

4. **Opérations CRUD - Personne Repository :**
   - Créez une classe de repository pour la table "Personne" ("PersonneRepository").
   - Implémentez des méthodes pour effectuer les opérations CRUD (Create, Read, Update, Delete) sur la table "Personne".
   - Implémentez la méthode permettant de récupérer toutes les adresses d'une personne.

5. **Opérations CRUD - Adresse Repository :**
   - Créez une classe de repository pour la table "Adresse" ("AdresseRepository").
   - Implémentez des méthodes pour effectuer les opérations CRUD sur la table "Adresse".

**Consignes générales :**

Pour simplifier au maximum l'exercice, nous n'utiliserons pas les types génériques et nous ne feront pas de tests unitaires. Les tests unitaires seront réalisés lors de la prochaine séance. Nous utiliseront directement le modèle comme objet "métier" pour aussi simplifier le fonctionnement.