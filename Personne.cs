namespace exercice_9;

public class Personne
{
    public int PersonneId { get; set; }
    public string Nom { get; set; }
    public string Prenom { get; set; }
    public DateTime DateNaissance { get; set; }
}
