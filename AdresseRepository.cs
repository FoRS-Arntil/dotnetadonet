using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;

namespace exercice_9;

public class AdresseRepository
{
    private readonly string connectionString;

    public AdresseRepository(string connectionString)
    {
        this.connectionString = connectionString;
    }

    // Méthode pour récupérer une adresse par son ID
    public Adresse ObtenirAdresseParId(int adresseId)
    {

        return null; // Aucune adresse trouvée pour l'ID spécifié
    }

    // Méthode pour créer une nouvelle adresse
    public void AjouterAdresse(Adresse adresse)
    {
        
    }

    // Méthode pour récupérer toutes les adresses
    public List<Adresse> ObtenirToutesLesAdresses()
    {
        List<Adresse> adresses = new List<Adresse>();


        return adresses;
    }

    // Méthode pour mettre à jour une adresse
    public void MettreAJourAdresse(Adresse adresse)
    {

    }

    // Méthode pour supprimer une adresse
    public void SupprimerAdresse(int adresseId)
    {

    }

    // Méthode utilitaire pour mapper les données du lecteur à l'objet Adresse
    public static Adresse MapAdresse(SQLiteDataReader reader)
    {
        return new Adresse
        {
            AdresseId = reader.GetInt32("AdresseId"),
            PersonneId = reader.GetInt32("PersonneId"),
            Rue = reader.GetString("Rue"),
            Ville = reader.GetString("Ville"),
            CodePostal = reader.GetString("CodePostal")
        };
    }
}
