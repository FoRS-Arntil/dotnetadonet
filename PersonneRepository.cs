using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;

namespace exercice_9;

public class PersonneRepository
{
    private readonly string connectionString;

    public PersonneRepository(string connectionString)
    {
        this.connectionString = connectionString;
    }

    // Méthode pour récupérer une personne par son ID
    public Personne ObtenirPersonneParId(int personneId)
    {
        
    }

    // Méthode pour créer une nouvelle personne
    public void AjouterPersonne(Personne personne)
    {
        
    }

    // Méthode pour récupérer toutes les personnes
    public List<Personne> ObtenirToutesLesPersonnes()
    {
        List<Personne> personnes = new List<Personne>();


        return personnes;
    }

    // Méthode pour récupérer toutes les adresses d'une personne
    public List<Adresse> ObtenirAdressesPersonne(int personneId)
    {
        List<Adresse> adresses = new List<Adresse>();


        return adresses;
    }

    // Méthode pour mettre à jour une personne
    public void MettreAJourPersonne(Personne personne)
    {
        
    }

    // Méthode pour supprimer une personne
    public void SupprimerPersonne(int personneId)
    {
        
    }

    // Méthode utilitaire pour mapper les données du lecteur à l'objet Personne
    public static Personne MapPersonne(SQLiteDataReader reader)
    {
        return new Personne
        {
            PersonneId = reader.GetInt32("PersonneId"),
            Nom = reader.GetString("Nom"),
            Prenom = reader.GetString("Prenom"),
            DateNaissance = reader.GetDateTime("DateNaissance")
        };
    }
}
