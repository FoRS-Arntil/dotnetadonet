﻿using System.Data.SQLite;

namespace exercice_9;
class Program
{
    static void Main()
    {
        // Générez la chaîne de connexion SQLite.
        string connectionString = $"Data Source=database.sqlite;Version=3;";

        AdresseRepository adresseRepository = new AdresseRepository(connectionString);
        PersonneRepository personneRepository = new PersonneRepository(connectionString);

        // Création d'une personne
        Personne nouvellePersonne = new Personne
        {
            Nom = "Doe",
            Prenom = "John",
            DateNaissance = new DateTime(1990, 1, 1)
        };

        personneRepository.AjouterPersonne(nouvellePersonne);
        Console.WriteLine("Personne ajoutée avec succès!");

        // Récupération de l'ID de la personne nouvellement ajoutée
        int personneId = nouvellePersonne.PersonneId;

        // Création d'une adresse liée à la personne
        Adresse nouvelleAdresse = new Adresse
        {
            PersonneId = personneId,
            Rue = "123 Rue de l'Adresse",
            Ville = "Ville",
            CodePostal = "12345"
        };

        adresseRepository.AjouterAdresse(nouvelleAdresse);
        Console.WriteLine("Adresse ajoutée avec succès!");

        nouvelleAdresse = new Adresse
        {
            PersonneId = personneId,
            Rue = "Rue des gaulois",
            Ville = "Liège",
            CodePostal = "4000"
        };

        adresseRepository.AjouterAdresse(nouvelleAdresse);
        Console.WriteLine("Adresse ajoutée avec succès!");

        // Affichage de toutes les adresses de la personne
        List<Adresse> adressesDeLaPersonne = personneRepository.ObtenirAdressesPersonne(personneId);

        Console.WriteLine($"Adresses de la personne (ID: {personneId}):");
        foreach (var adresse in adressesDeLaPersonne)
        {
            Console.WriteLine($"AdresseId: {adresse.AdresseId}, Rue: {adresse.Rue}, Ville: {adresse.Ville}, CodePostal: {adresse.CodePostal}");
        }

        // Mise à jour de l'adresse
        Adresse adresseAMettreAJour = adressesDeLaPersonne.FirstOrDefault();

        if (adresseAMettreAJour != null)
        {
            adresseAMettreAJour.Rue = "Nouvelle Rue";
            adresseAMettreAJour.CodePostal = "54321";

            adresseRepository.MettreAJourAdresse(adresseAMettreAJour);
            Console.WriteLine("Adresse mise à jour avec succès!");
        }

        Adresse adresseRecuperer = adresseRepository.ObtenirAdresseParId(adresseAMettreAJour.AdresseId);
        Console.WriteLine("Adresse mise à jours :");
        Console.WriteLine($"AdresseId: {adresseRecuperer.AdresseId}, Rue: {adresseRecuperer.Rue}, Ville: {adresseRecuperer.Ville}, CodePostal: {adresseRecuperer.CodePostal}");

        nouvellePersonne.Nom = "Dupont";
        nouvellePersonne.Prenom = "Marc";

        personneRepository.MettreAJourPersonne(nouvellePersonne);

        Personne personneRecuperer = personneRepository.ObtenirPersonneParId(nouvellePersonne.PersonneId);
        Console.WriteLine("Personne mise à jours :");
        Console.WriteLine($"Nom: {personneRecuperer.Nom}, Prenom: {personneRecuperer.Prenom}, DateNaissance: {personneRecuperer.DateNaissance}");

        Console.WriteLine("Affichage de toutes les adresses");
        List<Adresse> adresses = adresseRepository.ObtenirToutesLesAdresses();
        foreach (var adresse in adresses)
        {
            Console.WriteLine($"AdresseId: {adresse.AdresseId}, Rue: {adresse.Rue}, Ville: {adresse.Ville}, CodePostal: {adresse.CodePostal}");
        }

        List<Personne> personnes = personneRepository.ObtenirToutesLesPersonnes();
        Console.WriteLine("Affichage de toutes les personnes");
        foreach (var personne in personnes)
        {
            Console.WriteLine($"Nom: {personne.Nom}, Prenom: {personne.Prenom}, DateNaissance: {personne.DateNaissance}");
        }
        

        // Suppression de la personne (et de ses adresses associées en cascade)
        personneRepository.SupprimerPersonne(personneId);
        Console.WriteLine("Personne et adresses supprimées avec succès!");
    }
}

